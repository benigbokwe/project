# Simple product manager using my custom mvc #
== It utilises dotrine orm for database abstraction and object mapping ==

## Setup Steps ##

### virtual host config ###
        <VirtualHost *:80>
             ServerName dev.project
             DocumentRoot /path/to/project/public
             SetEnv APPLICATION_ENV "development"
             <Directory /path/to/project/public>
                 DirectoryIndex index.php
                 AllowOverride All
                 Require all granted
             </Directory>
         </VirtualHost>

 **system host**
 127.0.0.1  dev.project

 **mvc config**
    update database connection credentials in config/config.php file

**Update composer dependencies**
 - composer install

### Update schema ###
vendor/bin/doctrine orm:schema-tool:update --force --dump-sql

 **Open http://dev.project in browser**
     - View list of all products - if none exists, populate the product table

 *if you encounter any issues, email me at [ben.igbokwe@hotmail.com](Link URL)*