<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

define('DS', DIRECTORY_SEPARATOR);
define('BASEPATH', dirname(dirname(__FILE__)));

require_once dirname(__DIR__). '/bootstrap.php';
require_once dirname(__DIR__). '/config/router.php';
