<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 03/03/16
 * Time: 15:19
 */

namespace Core\mvc\mail;

class Message
{
    private $to;
    private $from;
    private $subject;
    private $body;
    private $encoder = 'UTF-8';

    public function getTo()
    {
        return $this->to;
    }
    
    public function setTo($recipientEmail, $recipientName)
    {
        $this->to = array($recipientEmail, $recipientName);
        
        return $this;
    }
    
    public function getFrom()
    {
        return $this->from;
    }

    public function setFrom($senderEmail, $senderName)
    {
        $this->from = array($senderEmail, $senderName);
        
        return $this;
    }
    
    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        
        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
        
        return $this;
    }
    
    public function getEncoding()
    {
        return $this->encoder;
    }

    public function setEncoding($encoder)
    {
        $this->encoder = $encoder;
        
        return $this;
    }
}