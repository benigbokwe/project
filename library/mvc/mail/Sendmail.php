<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 03/03/16
 * Time: 15:57
 */

namespace Core\mvc\mail;

class Sendmail
{
    public function send(Message $message)
    {
        //@todo process mail here
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=' . $message->getEncoding(). "\r\n";
        
        //@todo more validation to throw exception if not set
        if(is_array($message->getTo())) {
            list($recipientEmail, $recipientName) = $message->getTo();
        }

        if(is_array($message->getFrom())) {
            list($senderEmail, $senderName) = $message->getFrom();
        }

        // Additional headers
        $headers .= "To: $recipientEmail <$recipientName>" . "\r\n";
        $headers .= "From: $senderName <$senderEmail>" . "\r\n";

        // Mail it
        mail($recipientEmail, $message->getSubject(), $message->getBody(), $headers);
    }

}