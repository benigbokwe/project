<?php

namespace Core\mvc;

/**
 *
 * @author ben igbokwe
 */
class View
{
    protected $variables = array();
    protected $controller;
    protected $action;
    protected $bodyContent;
    public $section = array();
    private $layout = 'layout\default';

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    public function getLayout()
    {
        return str_replace('\\', DS, $this->layout);
    }

    /**
     * @param $controller
     * @param $action
     */
    public function __construct($controller, $action)
    {
        $this->controller = $controller;
        $this->action = $action;
    }

    /**
     * @param $array
     */
    public function setView($array)
    {
        foreach ($array as $key => $value) {
            $this->variables[$key] = $value;
        }
    }

    /**
     * @param $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
    * RenderBody
    */
    public function renderBody()
    {
        // if we have content, then deliver it
        if (!empty($this->bodyContent)) {
            echo $this->bodyContent;
        }
    }

    /**
     * Render section
     * @param $section
     */
    public function renderSection($section)
    {
        if (!empty($this->section) && array_key_exists($section, $this->section)) {
            echo $this->section[$section];
        }
    }

    /**
     * @param $template
     */
    public function setTemplate($template)
    {
        // extract the variables for view pages
        extract($this->variables);
        // the view path
        $path = BASEPATH . DS . 'module' . DS . 'application' . DS . 'src' . DS . 'views';

        $template = str_replace("\\", DS, $template);
                
        // start buffering
        ob_start();
        // render page content
         include($path . DS . $template . '.phtml');
        // get the body contents
        $this->bodyContent = ob_get_contents();
        // clean the buffer
        ob_end_clean();
        //@todo - fix this section
       // if(!empty($this->layout)){
            // include the template
           // include($this->layout);
          // ob_start();
          // include ($path . DS . $this->layout . '.phtml');
        //} else {
          //  ob_start();
            // just output the content
           // echo $this->bodyContent;  
        //}

        ob_start();
        $layout =  $this->getLayout();

        include($path . DS . $layout . '.phtml');
        
        // end buffer
        ob_end_flush();
    }

    /**
     * @param $path
     * @return string
     */
    public function basePath($path)
    {
        /**
         * @todo improve when deployed
         */
       return "http://".$_SERVER['SERVER_NAME'].'/booking/' . $path;
    }

    public function partial($template)
    {
        $path = BASEPATH . DS . 'module' . DS . 'application' . DS . 'views';

        $template = str_replace("\\", DS, $template);

        include($path . DS . $template . '.phtml');
    }
}
