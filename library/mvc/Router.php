<?php

namespace Core\mvc;

class Router
{
    protected $routeMaps = [];
    protected $route;

    /**
     * @param $route
     */
    public function __construct($route)
    {
        $this->route = $route;
    }

    /**
     * @param $name
     * @param array $routeParts
     * @return $this
     * @throws \Exception
     */
    public function add($name, array $routeParts)
    {
        if (!isset($this->routeMaps[$name])) {
            $this->routeMaps[$name] = $routeParts;
        } else {
            throw new \Exception("Route '{$name}' already defined");
        }

        return $this;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function run()
    {
        $routeMap = $this->findMatch($this->route);
        if (is_array($routeMap)) {
            $this->dispatch($routeMap, $this->route);
        } else {
            throw new \Exception('The resource you are looking for does not exist');
        }

        return true;
    }

    /**
     * @param $route
     * @return array
     * @throws \Exception
     */
    private function findMatch($route)
    {
        foreach ($this->routeMaps as $routeMap) {
            if (preg_match('/([{a-zA-Z}]+)[\/]([{a-zA-Z}]+)[\/]([{a-zA-Z}]+)\//', $routeMap['route']) &&
                preg_match('/([a-zA-Z]+)[\/]([a-zA-Z]+)[\/]([a-zA-Z0-9]+)\//', $route)) {
                return $routeMap;
            } elseif ($routeMap['route'] === $route) {
                return $routeMap;
            } elseif (preg_match('/([\w]+)\/([\w]+)\/([{\w}]+)/', $routeMap['route']) &&
                preg_match('/([\w]+)\/([\w]+)\/([\d]+)/', $route)) {
                $firstSlice  = array_slice(explode('/', $route), 2, 1);
                $secondSlice = array_slice(explode('/',  $routeMap['route']), 2, 1);

                if ($firstSlice == $secondSlice) {
                    return $routeMap;
                }
            }
        }

        return false;
        //throw new \Exception("404");
    }

    /**
     * @param $route
     * @return bool
     */
    public function redirectToRoute($route)
    {
        $map = $this->findMatch($route);

        if ($map) {
            $this->dispatch($map, $route);
        }

        return false;
    }

    /**
     * @param $routeMap
     * @param $url
     */
    private function dispatch($routeMap, $url)
    {
        $controller = $routeMap['controller'];
        $action = $routeMap['action'] . 'Action';

        $params = $this->parseParams($url);
        call_user_func_array([new $controller, $action], [$params]);
    }

    /**
     * @param $url
     * @return array
     */
    private function parseParams($url)
    {
        return  array_slice(explode('/', $url), 1);
    }
}
