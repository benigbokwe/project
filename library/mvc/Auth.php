<?php
namespace Core\mvc;

/**
 * Description of Auth
 *
 * @author ben
 */
class Auth extends Model
{
    /**
    *
    * @var mixed
    */
    private static $identity;

    /**
     * 
     * @param mixed $identity
     */
    public static function setIdentity($identity)
    {
        static::$identity = $identity;
    }

    /**
     * 
     * @return mixed
     */
    public static function getIdentity()
    {
        return static::$identity;
    }

    /**
     * 
     * @param string $username
     * @param string $password
     * @param int $status
     * @return boolean
     */
    public static function attempt($username, $password, $status = 1)
    {
        $sql =  'SELECT * FROM `user` '
                . 'WHERE `user_name` = :username '
                . 'AND `status` = :status';

        $options = [
            'cost' => 12,
        ];

       // $hash = password_hash($password, PASSWORD_BCRYPT, $options);

        $params = 
            array(':username' => $username,
                  ':status' => $status
            );

        $self = new self();

        $identity = $self->fetch($sql, $params);

        if(is_array($identity) && isset($identity['password'])) {
            $verify = password_verify($password, $identity['password']);
            
            // for security reasons, unset password
            unset($identity['password']);
            
            if($verify) {
               static::setIdentity($identity);
               
               return true;
            }
 
            //@todo register session
            return false;
        }
        
        return false;
    }

   /**
    * 
    * @return array|null
    */
    public static function getSession()
    {
        if(!isset($_SESSION)) { 
            session_start(); 
        }
        
        if(is_array(static::getIdentity())) {
           $_SESSION['user'] = static::getIdentity();
        }

        return $_SESSION;
    }

    /**
     * Log out the current user
     *
     * @return  void
     */
    public static function logout()
    {
        static::$user = null;

        $_SESSION = array();
        unset($_SESSION['user_id']);
        session_destroy();
    }

    /**
     * Checks if a user is currently logged in
     *
     * @return bool
     */
    public static function check()
    {
        return isset($_SESSION['user_id']);
    }

    /**
     * Checks if the current user is a guest
     *
     * @return  bool
     */
    public static function guest()
    {
        return !isset($_SESSION['user_id']);
    }
}
