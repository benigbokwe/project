<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 29/02/16
 * Time: 12:00
 */

namespace Core\mvc;


class JsonModel
{
    /**
     * @param $content
     * @return string
     */
    public static function encode($content)
    {
        if(is_array($content)) {
            return json_encode($content);
        } else {
            return json_encode(array());
        }
    }

    /**
     * @param $content
     * @param bool $toArray
     * @return mixed
     */
    public static function decode($content, $toArray = true)
    {
        return json_decode($content, $toArray);
    }
}