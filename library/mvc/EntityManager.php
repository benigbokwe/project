<?php

namespace Core\mvc;

use config\config;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager as ORMEntityManager;

class EntityManager
{
    /**
     * @return ORMEntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    public static function create()
    {
        $paths = array( dirname(dirname(__DIR__))."/module/application/src/Entity");
        $isDevMode = false;

        $db = config::get('db');

        $dbParams = [
            'driver'   => $db['driver'],
            'user'     => $db['user'],
            'password' => $db['password'],
            'dbname'   => $db['dbname'],
        ];

        // obtaining the entity manager
        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        return ORMEntityManager::create($dbParams, $config);
    }
}
