<?php

namespace Core\mvc;

/**
 * Class AbstractController
 * @package Core\mvc
 */
abstract class AbstractController
{
    protected $model;
    protected $controller;
    protected $action;
    public $view;
    public $table;
    public $id;
    public $userValidation;

    public function __construct($controller = "Controller", $action = "index")
    {
        // construct MVC
        $this->controller = $controller;
        $this->action = $action;
        // initialise the template class

        $this->view = new View($controller, $action);
    }

    /**
     * @param $action
     * @param bool|false $controller
     * @param array $params
     * @return mixed
     */
    public function redirectToAction($action, $controller = false, $params = array())
    {
        if ($controller === false) {
            $controller = get_called_class();
        } elseif (is_string($controller) && class_exists($controller . 'Controller')) {
            $controller = $controller . 'Controller';
        }

        return call_user_func_array(array($controller, $action), $params);
    }

    /**
     * @param $route
     * @todo improve
     */
    public function redirectRoute($route)
    {
        header("Location:  http://" . $_SERVER['SERVER_NAME'] . DS . $route);

        exit();
    }

    /**
     * @param array $params
     */
    public function unknownAction($params = array())
    {
        // feed 404 header to the client
        header("HTTP/1.0 404 Not Found");
        // find custom 404 page

        $this->setTemplate('layout\404');
    }

    /**
     * @param array $array
     */
    public function setView(array $array)
    {
        // set the parameters to the template class
        $this->view->setView($array);
    }

    /**
     * @param $layout
     */
    public function setLayout($layout)
    {
        $this->view->setLayout($layout);
    }

    /**
     * @param $template
     */
    public function setTemplate($template)
    {
        $this->view->setTemplate($template);
    }

    /**
     * Returns the template result
     */
    public function view()
    {
        // dispatch the result of the template class
        return $this->view;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return EntityManager::create();
    }
}
