<?php
namespace Core\mvc;

/**
 *
 * @author ben igbokwe
 */
class Helpers
{
    /**
     * @param $string
     * @return string
     */
    public static function addQuotes($string)
    {
        return sprintf("'%s'", $string);
    }

    /**
     * @param $string
     * @return string
     */
    public static function backTicks($string)
    {
        return sprintf("`%s`", $string);
    }
}
