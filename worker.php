#!/usr/bin/env php
<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 05/08/16
 * Time: 14:52
 */
ini_set('display_errors', 1);
error_reporting(E_ALL);

define('DS', DIRECTORY_SEPARATOR);
define('BASEPATH', dirname(__FILE__));

require_once __DIR__ . '/vendor/autoload.php';

$worker = new \Application\Queue\WorkerReceiver();

$worker->listen();