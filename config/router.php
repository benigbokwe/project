<?php
/**
 * Created by PhpStorm.
 * User: ben
 */

$router = new \Core\mvc\Router($_SERVER['REQUEST_URI']);

$router
    ->add('home-page',
        [
            'route'      => '/',
            'controller' => '\Application\Controller\IndexController',
            'action'     => 'index'
        ]
    )
    ->add('add-product-item',
        [
            'route'      => '/product/add',
            'controller' => '\Application\Controller\ProductController',
            'action'     => 'add'
        ]
    )
    ->add('list-product-item',
        [
            'route'      => '/product/list',
            'controller' => '\Application\Controller\ProductController',
            'action'     => 'list'
        ]
    )
    ->add('edit-product-item',
        [
            'route'      => '/product/edit/{id}',
            'controller' => '\Application\Controller\ProductController',
            'action'     => 'edit'
        ]
    )
    ->add('delete-product-item',
        [
            'route'      => '/product/delete/{id}',
            'controller' => '\Application\Controller\ProductController',
            'action'     => 'delete'
        ]
    )
    ->add('contact-index',
        [
            'route'      => '/contact',
            'controller' => '\Application\Controller\ContactController',
            'action'     => 'index'
        ]
    )
    ->add('contact-send',
        [
            'route'      => '/contact/send',
            'controller' => '\Application\Controller\ContactController',
            'action'     => 'send'
        ]
    );

//finally we dispatch the output
$router->run();