<?php

namespace config;

class config
{
    /**
     * @var array
     */
    private static $options = [
        'db' => [
            'host'      => '127.0.0.1',
            'dbname'    => 'test_db',
            'user'      => 'root',
            'password'  => 'root',
            'driver'    => 'pdo_mysql'
        ],
        'endpoint_url'  => 'http://purse-devel.isoftbet.com/wallet/index.php',
        'login'         => 'isoftbet',
        'password'      => 'cujeePhah6',
        'secret_key'    => '1b9a80606d74d3da6db2f1274557e644'
    ];

    /**
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        return self::$options[$key];
    }
}
