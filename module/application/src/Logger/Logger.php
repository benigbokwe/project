<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 12/08/16
 * Time: 14:37
 */

namespace Application\Logger;

class Logger
{
    /**
     * @param $output
     * @return bool
     */
    public static function persistResponse($output)
    {
        $directory = BASEPATH . DS . 'logs';

        if(!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        if (is_array($output)) {
            $output = json_encode($output);
        }

        $timestamp = time();

        $file = $directory . '/' . date('Y-m-d') . '.txt';

        $userTime  = gmdate("Y-m-d\\TH:i:s", $timestamp);
        $response  = $userTime . "\n" . "==================================\n";
        $response .= $output . "\n\n";

        // Write the contents to the file,
        // using the FILE_APPEND flag to append the content to the end of the file
        // and the LOCK_EX flag to prevent anyone else writing to the file at the same time
        if (file_put_contents($file, $response, FILE_APPEND | LOCK_EX)) {
            return true;
        }

        echo "** Not saved ** \n";

        return false;
    }
}
