<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 11/08/16
 * Time: 12:40
 */

namespace Application\Request;


class InitRequest extends Request
{

    /**
     * @param InitRequest $request
     * @return array
     */
    public function composeIniRequest(InitRequest $request)
    {
        $this->requestArray['state'] = 'single';
        $this->initAction($request);

        return $this->requestArray;
    }

    /**
     * @param InitRequest $request
     * @return array
     */
    protected function initAction(InitRequest $request)
    {
        $this->requestArray['action'] = array(
            'command'    => 'init',
            'parameters' => array(
                'token' => $request->getToken(),
                'country' => $request->getCountry()
            ),
        );

        return $this->requestArray;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->requestArray;
    }
}