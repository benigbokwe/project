<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 11/08/16
 * Time: 12:26
 */

namespace Application\Request;


class Request
{
    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $allowOpenRounds;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var mixed
     */
    private $skinId;

    /**
     * @var mixed
     */
    private $playerId;

    /**
     * @var mixed
     */
    private $operator;

    /**
     * @var array
     */
    private $action;

    /**
     * @var string
     */
    private $country;

    /**
     * @var array
     */
    protected $requestArray;

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param $sessionId
     * @return $this
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
        $this->requestArray['sessionid'] = $sessionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return string
     */
    public function getAllowOpenRounds()
    {
        return $this->allowOpenRounds;
    }

    /**
     * @param $allowOpenRounds
     * @return $this
     */
    public function setAllowOpenRounds($allowOpenRounds)
    {
        $this->allowOpenRounds = $allowOpenRounds;
        $this->requestArray['allow_open_rounds'] = $allowOpenRounds;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        $this->requestArray['currency'] = $currency;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSkinId()
    {
        return $this->skinId;
    }

    /**
     * @param $skinId
     * @return $this
     */
    public function setSkinId($skinId)
    {
        $this->skinId = $skinId;
        $this->requestArray['skinid'] = $skinId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * @param $playerId
     * @return $this
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;
        $this->requestArray['playerid'] = $playerId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param $operator
     * @return $this
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
        $this->requestArray['operator'] = $operator;

        return $this;
    }

    /**
     * @return array
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param $action
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;
        $this->requestArray['action'] = $action;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }
}