<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 27/07/16
 * Time: 12:20
 */

namespace Application\Controller;


use Application\Queue\WorkerSender;
use Application\Request\InitRequest;
use Core\mvc\AbstractController;

class ContactController extends AbstractController
{
    public function indexAction()
    {
        $this->setTemplate('contact\index');

    }

    public function sendAction()
    {
        if(array_key_exists('send', $_POST)) {

            $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $array = $this->prepareInitRequest($post);
            if(is_array($array)){
               $json         = json_encode($array);
               $workerSender = new WorkerSender();
               $workerSender->execute($json);
            }

            $this->setTemplate('contact\success');
        } else {
            $this->redirectRoute('contact');
        }
    }

    /**
     * @param $post
     * @return array
     */
    public function prepareInitRequest($post)
    {
        $sessionId          = $this->generateSessionId();
        $token              = $post['token'];
        $currency           = $post['currency'];
        $allow_open_rounds  = $post['allow_open_rounds'];
        $skinId             = $post['skin_id'];
        $playerId           = $post['player_id'];
        $country            = $post['country'];

        $initRequest = new InitRequest();
        $initRequest
            ->setPlayerId($playerId)
            ->setSkinId($skinId)
            ->setAllowOpenRounds($allow_open_rounds)
            ->setCurrency($currency)
            ->setCountry($country)
            ->setToken($token)
            ->setSessionId($sessionId);

        return $initRequest->composeIniRequest($initRequest);
    }

    /**
     * @param int $length
     * @return string
     */
    private function generateSessionId($length = 32)
    {
        $str = 'abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for ($i = 0, $string = ''; $i < $length; $i++)
            $string .= substr($str, mt_rand(0, strlen($str) - 1), 1);
        return $string;
    }

}