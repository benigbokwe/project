<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 18/05/16
 * Time: 18:11
 */

namespace Application\Controller;

use Application\Entity\Product;
use Core\mvc\AbstractController;

class ProductController extends AbstractController
{
    public function addAction()
    {
        if(array_key_exists('add', $_POST)) {
            $post    = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $product = new Product();

            foreach ($post as $name => $value) {
                $setter = 'set' . ucfirst($name);
                if (method_exists($product, $setter)) {
                    call_user_func_array([$product, $setter], [$value]);
                }
            }

            $entityManager = $this->getEntityManager();

            $entityManager->persist($product);
            $entityManager->flush();

            $this->redirectRoute('product/list');
        }

        $this->setTemplate('product\add');
    }

    public function listAction()
    {
        $productRepository = $this->getEntityManager()->getRepository('Application\Entity\Product');
        $products = $productRepository->findAll();

        $this->setView(
            [
                'products' => $products
            ]
        );

        $this->setTemplate('product\list');
    }

    /**
     * @param $params
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function editAction($params)
    {
        @list($controller, $action, $id) = $params;

        $entityManager = $this->getEntityManager();
        $product = $entityManager->find('Application\Entity\Product', $id);

        if(array_key_exists('edit', $_POST) && ($product instanceof Product)) {

            $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            foreach ($post as $name => $value) {
                if ($name != 'id') {
                    $setter = 'set' . ucfirst($name);
                    if (method_exists($product, $setter)) {
                        call_user_func_array([$product, $setter], [$value]);
                    }
                }
            }

            $entityManager->flush();
            $this->redirectRoute('product/list');
        }

        $this->setView(
            [
                'product' => $product,
                'id'      => $id
            ]
        );

        $this->setTemplate('product\edit');
    }

    /**
     * @param $params
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function deleteAction($params)
    {
        @list($controller, $action, $id) = $params;

        $entityManager = $this->getEntityManager();
        $product = $entityManager->find('Application\Entity\Product', $id);

        if($product instanceof Product) {
            $entityManager->remove($product);
            $entityManager->flush();
        }

        $this->redirectRoute('product/list');
    }
}