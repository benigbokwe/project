<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 18/05/16
 * Time: 18:15
 */
namespace Application\Controller;

use config\config;
use Core\mvc\AbstractController;
use Core\mvc\JsonModel;

class IndexController extends AbstractController
{
    /**
     * @return string
     */
    public function indexAction()
    {
       $this->setTemplate('index\index');
    }
}