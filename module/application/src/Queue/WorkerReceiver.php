<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 05/08/16
 * Time: 10:23
 */

namespace Application\Queue;

use Application\Curl\CurlWrapper;
use Application\Logger\Logger;
use config\config;
use PhpAmqpLib\Message\AMQPMessage;

class WorkerReceiver extends Worker
{
    /**
     * @var int
     */
    private $startTime;

    /**
     * @var int
     */
    private $ttl = 36000;

    /**
     * @return void
     */
    public function listen()
    {
        // set the third parameter to 'true' to make RabbitMQ durable
        $this->channel->queue_declare('transaction_queue', false, true, false, false);

        echo ' * Waiting for messages. To exit press CTRL+C', "\n";

        // record the time worker started
        $this->startTime = time();

        $this->channel->basic_qos(null, 1, null);

        $this->channel->basic_consume(
            'transaction_queue',
            '',
            false,
            false,
            false,
            false,
            array($this, 'consume')
        );

        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }

        $this->channel->close();
        $this->connection->close();
    }

    /**
     * @param AMQPMessage $message
     */
    public function consume(AMQPMessage $message)
    {
        echo "** Message received ** \n";

        // stop consuming when ttl is reached
        if (($this->startTime + $this->ttl) < time()) {
            echo "** Message ttl reached ** \n";
            $this->channel->basic_cancel($message->delivery_info['consumer_tag']);

            exit;
        }


        // do whatever you have to do with your message
        $result = $this->executeWorker($message->body);

        if ($result instanceof CurlWrapper) {

            // check response and repeat on error
            $response = $result->getRequestResponse();

            // save to log file
            Logger::persistResponse($response);

            // handle connection timeout || read timeout
            if ($result->getErrorNumber() == 7 || $result->getErrorNumber() == 28) {
                // delay for 2 seconds then exit - This allows the next consumer worker to handle the task
                sleep(2);
                // stop consuming when we have connection timeout
                $this->channel->basic_cancel($message->delivery_info['consumer_tag']);
                echo "** Message rolled backed ** \n";

                exit;
            }

            echo "** Message processed** \n";

            // tell rabbitmq that message is completed
            $channel = $message->delivery_info['channel'];
            $channel->basic_ack($message->delivery_info['delivery_tag']);
        }
    }

    /**
     * @param $body
     * @return CurlWrapper
     */
    protected function executeWorker($body)
    {
        $url       = config::get('endpoint_url');
        $secretKey = config::get('secret_key');

        $purseUrl = $this->purseUrl($body, $url, $secretKey);

        $curl = new CurlWrapper();
        $curl->setEndPointUrl($purseUrl);
        $curl->setPostFields($body);

        $curl->executeRequest();

        return $curl;
    }

    /**
     * @param $data
     * @param $url
     * @param $secretKey
     * @return string
     */
    protected function purseUrl($data, $url, $secretKey)
    {
        $build = hash_hmac('SHA256', $data, trim($secretKey));
        $u = $url . '?hash=' . $build;
        return $u;
    }
}
