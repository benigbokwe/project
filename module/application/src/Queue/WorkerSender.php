<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 05/08/16
 * Time: 10:10
 */
namespace Application\Queue;


use PhpAmqpLib\Message\AMQPMessage;

class WorkerSender extends Worker
{
    /**
     * @param $data
     * @return bool|string
     */
    public function execute($data)
    {
        try {
            // set the third parameter to 'true' to make RabbitMQ durable
            $this->channel->queue_declare(
                'transaction_queue',
                false,
                true,
                false,
                false
            );

            //$json = json_encode($data);

            $msg = new AMQPMessage($data, array('delivery_mode' => 2));
            $this->channel->basic_publish($msg, '', 'transaction_queue');

            $this->channel->close();
            $this->connection->close();
        } catch(\Exception $ex){
            return $ex->getMessage();
        }

        return true;
    }
}