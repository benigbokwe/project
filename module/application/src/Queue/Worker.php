<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 05/08/16
 * Time: 10:12
 */

namespace Application\Queue;


use PhpAmqpLib\Connection\AMQPStreamConnection;

abstract class Worker
{
    protected $connection;
    protected $channel;

    public function __construct()
    {
        $this->connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $this->channel    = $this->connection->channel();
    }
}