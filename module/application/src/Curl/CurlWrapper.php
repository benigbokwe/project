<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 11/08/16
 * Time: 12:00
 */

namespace Application\Curl;


class CurlWrapper
{
    /**
     * @var string
     */
    private $endPointUrl;

    /**
     * @var string
     */
    private $postFields;

    /**
     * @var int
     */
    protected $connectionTimeout = 2;

    /**
     * @var int
     */
    protected $timeout = 5;

    /**
     * @var
     */
    private $errorNumber;

    /**
     * @var Response
     */
    public $response;

    /**
     * @return mixed
     */
    public function getEndPointUrl()
    {
        return $this->endPointUrl;
    }

    /**
     * @param $endPointUrl
     * @return $this
     */
    public function setEndPointUrl($endPointUrl)
    {
        $this->endPointUrl = $endPointUrl;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostFields()
    {
        return $this->postFields;
    }

    /**
     * @param $postFields
     * @return $this
     */
    public function setPostFields($postFields)
    {
        $this->postFields = $postFields;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getErrorNumber()
    {
        return $this->errorNumber;
    }

    /**
     * @param $errorNumber
     * @return $this
     */
    public function setErrorNumber($errorNumber)
    {
        $this->errorNumber = $errorNumber;

        return $this;
    }

    /**
     * sends request to api endpoint
     */
    public function executeRequest()
    {
        $ch = curl_init($this->getEndPointUrl());
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostFields());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->connectionTimeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($this->getPostFields()))
        );

        $response = curl_exec($ch);

        if($response === false) {
            $this->errorNumber = curl_errno($ch);
            $this->response()->setRequestResponse('Curl error: ' . curl_error($ch));
        } else {
            $this->response()->setRequestResponse($response);
        }

        // close handle
        curl_close($ch);

        return $this;
    }

    /**
     * @return mixed
     */
    public function response()
    {
        $this->response = new Response();
        return $this->response;
    }

    public function getRequestResponse()
    {
        return $this->response->getRequestResponse();
    }
}