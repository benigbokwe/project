<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 11/08/16
 * Time: 12:01
 */

namespace Application\Curl;


class Response
{
    private $requestResponse;

    /**
     * @return mixed
     */
    public function getRequestResponse()
    {
        return $this->requestResponse;
    }

    /**
     * @param mixed $requestResponse
     */
    public function setRequestResponse($requestResponse)
    {
        $this->requestResponse = $requestResponse;
    }
}