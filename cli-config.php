<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Core\mvc\EntityManager;

$entityManager = EntityManager::create();

return ConsoleRunner::createHelperSet($entityManager);